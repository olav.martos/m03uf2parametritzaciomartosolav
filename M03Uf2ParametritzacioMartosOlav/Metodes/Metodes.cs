﻿/*
 * AUTHOR: Olav Martos
 * DATE: 14/12/2022
 * DESCRIPTION: Conjunt de metodes per practicar la parametrització
 */


using System;

namespace Metodes
{
    class Metodes
    {
        static void Main()
        {
            var menu = new Metodes();
            menu.Menu();
        }

        public void Menu()
        {
            var option = "";
            string[] exercise = { "Sortir del menu", "RIGHTTRIANGLESIZE", "Lamp", "CAMPSITEORGANIZER", "BASICROBOT", "THREEINAROW", "SQUASHCOUNTER" };
            do
            {
                Console.Clear();
                showExercise(exercise);
                Console.Write("Escull una opcio: ");

                option = Console.ReadLine();
                switch (option)
                {
                    case "1":
                        Console.Clear();
                        RIGHTTRIANGLESIZE();
                        break;
                    case "2":
                        Console.Clear();
                        Lamp();
                        break;
                    case "3":
                        Console.Clear();
                        CAMPSITEORGANIZER();
                        break;
                    case "4":
                        Console.Clear();
                        BASICROBOT();
                        break;
                    case "5":
                        Console.Clear();
                        THREEINAROW();
                        break;
                    case "6":
                        Console.Clear();
                        SQUASHCOUNTER();
                        break;
                    case "0":
                    case "END":
                    case "end":
                    case "cierrate":
                    case "ADIOS":
                    case "adios":
                    case "EXIT":
                    case "Exit":
                    case "exit":
                    case "FIN":
                    case "fin":
                    case "Fin":
                        Console.Clear();
                        endExercise();
                        Environment.Exit(0);
                        break;
                    default:
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("Opció incorrecta!");
                        Console.ForegroundColor = ConsoleColor.White;
                        break;
                }
                Console.ReadLine();
            } while (option != "0");

        }

        public void showExercise(string[] ex)
        {
            for (int i = 0; i < ex.Length; i++) Console.WriteLine($"{i}. - {ex[i]}");
        }

        public void endExercise()
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Pulsa una tecla per terminar");
            Console.ForegroundColor = ConsoleColor.White;
            return;
        }



        /* Ens digui l'àrea i el perímetre d'una llista de triangles rectangles.
         * L'usuari introdueix el número de triangles
         * L'usuari entra la base i l'altura del triangle
         * Imprimeix la superfície i el perímetre dels diferents triangles.
         * Nota: No pots imprimir els triangles fins a que s'hagin llegit tots.
         */
        public void RIGHTTRIANGLESIZE()
        {
            Console.Write("Introdueix el numero de triangles que vols calcular: ");
            int numTriangles = Convert.ToInt32(Console.ReadLine());
            int numAuxTriangles;
            if (numTriangles == 1) numAuxTriangles = 2;
            else numAuxTriangles = numTriangles;
            double[,] triangles = new double[1, 2];

            triangleSize(triangles);
            areaPerimeter(triangles, numTriangles);
            endExercise();
        }

        /*Part de RIGHTTRIANGLESIZE*/
        static void triangleSize(double[,] tri)
        {
            for (int i = 0; i < tri.GetLength(0); i++)
            {
                for (int j = 0; j < tri.GetLength(1); j++)
                {
                    Console.Write($"Introdueix la medida d'el triangle {i}: ");
                    tri[i, j] = Convert.ToDouble(Console.ReadLine());
                }
            }

            return;
        }

        /*Part de RIGHTTRIANGLESIZE*/
        static void areaPerimeter(double[,] tri, int trianglesNum)
        {
            for (int l = 0, i = 0; l < trianglesNum; l++, i++)
            {
                int j = 0, k = 1;
                Console.WriteLine($"Un triangle de {tri[i, j]} x {tri[i, k]} té {(tri[i, j] * tri[i, k]) / 2} d'area i {tri[i, j] * 3} de perimetre");
            }
            return;
        }



        /* Simuli una làmpada, amb les accions:
         * TURN ON: Encén la làmpada
         * TURN OFF: Apaga la làmpada
         * TOGGLE: Canvia l'estat de la làmpada
         * Després de cada acció mostra si la làmpada està encesa.
         */
        public void Lamp()
        {
            int num = -1;
            string act = " ";
            bool light = false;

            Console.ForegroundColor = ConsoleColor.DarkCyan;
            Console.WriteLine("TURN ON: Encén la làmpada\n" +
                "TURN OFF: Apaga la làmpada\n" +
                "TOGGLE: Canvia l'estat de la làmpada\n" +
                "END: Finalitza el programa\n");
            Console.ResetColor();
            Console.Write("Escriu una acció: ");
            act = Console.ReadLine();
            do
            {
                if (act.ToUpper() == "TURN ON") num = 1;
                else if (act.ToUpper() == "TURN OFF") num = 0;
                else if (act.ToUpper() == "TOGGLE") num = 2;

                switch (num)
                {
                    case 0:
                        light = off(light);
                        Console.WriteLine(light);
                        break;
                    case 1:
                        light = on(light);
                        Console.WriteLine(light);
                        break;
                    case 2:
                        light = lightInverse(light);
                        Console.WriteLine(light);
                        break;
                    default:
                        Console.WriteLine("Error");
                        break;
                }

                Console.Write("Escriu una acció: ");
                act = Console.ReadLine();

            } while (act.ToUpper() != "END");
            endExercise();
        }

        // Lights of lamp: OFF
        bool off(bool light)
        {
            return false;
        }

        // Lights of lamp: ON
        bool on(bool light)
        {
            return true;
        }

        // Lights of lamp: TOGGLE
        bool lightInverse(bool light)
        {
            return !light;
        }



        /*
         * Gestionar un camping. Volem tenir controlat quantes parcel·les tenim plenes i quanta gent tenim.
         * Quan s'ocupa una parcel·la l'usuari introduirà el número de persones i el nom de la reserva
         * ENTRA 2 Maria
         * 
         * Quan es buidi una parcel·la l'usuari introduirà el nom de la reserva
         * MARXA Maria
         * 
         * Per tancar el programa l'usuari introduirà el nom END
         * Cada cop que marxi alguna persona, imprimeix el número total de persones que hi ha, i el número de parcel·les plenes.
         */
        public void CAMPSITEORGANIZER()
        {
            /*
             * COMMAND TYPE OPTIONS:
             * 1 -> SET PARCEL
             * 2 -> DROP PARCEL
             * -1 -> END PROGRAM
             */
            int maxParcel = 1000;

            string[] parcelNames = new string[maxParcel];
            int[] parcelNums = new int[maxParcel];

            int commandType;
            string command;

            string parcelName = "";
            int parcelNum = 0;

            int index = -1;
            int parcels = 0;
            do
            {
                Console.Write("Escriu l'accio: ");
                command = Console.ReadLine();
                commandType = getCommand(command, ref parcelName, ref parcelNum);

                switch (commandType)
                {
                    case 1:
                        index++;
                        parcels++;
                        setParcel(parcelName, parcelNum, parcelNames, parcelNums, index);
                        printParcel(parcelNums, parcels);
                        break;
                    case 2:
                        parcels--;
                        dropParcel(parcelName, parcelNames, parcelNums);
                        printParcel(parcelNums, parcels);
                        break;
                    case -1:
                        endExercise();
                        break;
                    default:
                        Console.WriteLine("Error!");
                        break;
                }

            } while (commandType != -1);
        }

        private int getCommand(string command, ref string parcelName, ref int parcelNum)
        {
            // Split the command
            string[] spliCom = command.Split(' ');

            if (spliCom[0].ToUpper() == "ENTRA")
            {
                parcelNum = Convert.ToInt32(spliCom[1]);
                parcelName = spliCom[2];
                return 1;
            }
            else if (spliCom[0].ToUpper() == "MARXA")
            {
                parcelName = spliCom[1];
                return 2;
            }
            else if (spliCom[0].ToUpper() == "END") return -1;
            else return 0;
        }

        private void setParcel(string parcelName, int parcelNum, string[] parcelNames, int[] parcelNums, int index)
        {
            parcelNames[index] = parcelName;
            parcelNums[index] = parcelNum;
        }

        private void dropParcel(string parcelName, string[] parcelNames, int[] parcelNums)
        {
            for (int i = 0; i < parcelNames.Length; i++)
            {
                if (parcelName == parcelNames[i])
                {
                    parcelNums[i] = 0;
                    parcelNames[i] = " ";
                }
            }
        }

        void printParcel(int[] parcelNums, int parcels)
        {
            // Mostrar por pantalla el numero de persones y de parcelas ocupadas
            int people = 0;
            for (int i = 0; i < parcelNums.Length; i++)
            {
                people += parcelNums[i];
            }

            Console.WriteLine($"Parcela: {parcels}");
            Console.WriteLine($"Persones: {people}");
        }



        /*
         * Permet moure un robot en un planell 2D. 
         * El robot ha de guardar la seva posició (X, Y) i la velocitat actual. Per defecte, la posició serà (0, 0) i la velocitat és 1. La velocitat indica la distància que recorre el robot en cada acció. 
         * El robot té les següents accions: 
         * DALT: El robot es mou cap a dalt (tenint en compte la velocitat). 
         * BAIX: El robot es mou cap a baix (tenint en compte la velocitat). 
         * DRETA: El robot es mou cap a la dreta (tenint en compte la velocitat). 
         * ESQUERRA: El robot es mou cap a l’esquerra (tenint en compte la velocitat). 
         * ACELERAR: El robot aumenta en 0.5 la velocitat. La velocitat màxima és 10. 
         * DISMINUIR: El robot aumenta en 0.5 la velocitat. La velocitat mínima és 0. 
         * POSICIO: El robot imprimeix la seva posició. 
         * VELOCITAT: El robot imprimeix la seva velocitat. 
         * La simulació acaba amb l'acció END.
         */
        public void BASICROBOT()
        {
            double positionX = 0.0;
            double positionY = 0.0;
            double velocity = 1.0;
            Console.Write("Escriu les accions que vols que el robot faci: ");
            string action = Console.ReadLine();
            int act = -1;

            do
            {
                if (action.ToUpper() == "DALT") act = 1;
                if (action.ToUpper() == "BAIX") act = 2;
                if (action.ToUpper() == "DRETA") act = 3;
                if (action.ToUpper() == "ESQUERRA") act = 4;
                if (action.ToUpper() == "ACCELERAR") act = 5;
                if (action.ToUpper() == "DISMINUIR") act = 6;
                if (action.ToUpper() == "POSICIO") act = 7;
                if (action.ToUpper() == "VELOCITAT") act = 8;

                switch (act)
                {
                    case 1:
                        positionY = DALT(positionY, velocity);
                        break;
                    case 2:
                        positionY = BAIX(positionY, velocity);
                        break;
                    case 3:
                        positionX = DRETA(positionX, velocity);
                        break;
                    case 4:
                        positionX = ESQUERRA(positionX, velocity);
                        break;
                    case 5:
                        velocity = ACCELERAR(velocity);
                        break;
                    case 6:
                        velocity = DISMINUIR(velocity);
                        break;
                    case 7:
                        Console.WriteLine($"La posició del robot és ({positionX}, {positionY})");
                        break;
                    case 8:
                        Console.WriteLine($"La velocitat del robot és {velocity}");
                        break;
                }

                Console.Write("Escriu les accions que vols que el robot faci: ");
                action = Console.ReadLine();
            } while (action.ToUpper() != "END");
            endExercise();

        }

        double DALT(double positionY, double velocity)
        {
            positionY += velocity;
            return positionY;
        }

        double BAIX(double positionY, double velocity)
        {
            positionY -= velocity;
            return positionY;
        }

        double DRETA(double positionX, double velocity)
        {
            positionX += velocity;
            return positionX;
        }

        double ESQUERRA(double positionX, double velocity)
        {
            positionX -= velocity;
            return positionX;
        }

        double ACCELERAR(double velocity)
        {
            velocity += 0.5;
            return velocity;
        }

        double DISMINUIR(double velocity)
        {
            velocity -= 0.5;
            return velocity;
        }



        /*
         * Jugar dos usuaris al tres en ratlla.
         * Es juga amb un taulell de 3x3. Cada jugador col·loca una peça al taulell alternativament. Guanya qui és capaç de col·locar-les formant una línia de 3, ja sigui vertical, horitzontal o diagonal.
         * El programa a desenvolupar haurà de mostrar el taulell per pantalla i demanar la tirada al primer jugador.
         * El jugador escriurà dos números, el primer correspon a les columnes i el segon a les files. S'anirà demanant el torn alternativament a cada jugador fins que algun dels dos faci el tres en ratlla o bé fins que el taulell estigui ple.
         * Les fitxes d'un jugador les representarem amb una X i les de l'altre amb una O.
         * Al final es mostrarà un missatge dient el resultat de la partida: guanyen les X, guanyen les O o empat.
         */
        public void THREEINAROW()
        {
            char[,] tauler = new char[3, 3];
            bool victory = false;
            for (int i = 0; i < tauler.GetLength(0); i++)
            {
                for (int j = 0; j < tauler.GetLength(1); j++)
                {
                    tauler[i, j] = '.';
                }
            }
            showMatrix(tauler);
            while (victory == false)
            {
                int dotExistence = 0;

                firstPlayer(tauler);
                showMatrix(tauler);
                compareFirst(tauler, victory);

                for (int row = 0; row < tauler.GetLength(0); row++)
                {
                    for (int col = 0; col < tauler.GetLength(1); col++)
                    {
                        if (tauler[row, col] == '.')
                        {
                            dotExistence++;
                        }
                    }
                }
                if (dotExistence == 0)
                {
                    tie(victory);
                }

                secondPlayer(tauler);
                showMatrix(tauler);
                compareSecond(tauler, victory);

                for (int row = 0; row < tauler.GetLength(0); row++)
                {
                    for (int col = 0; col < tauler.GetLength(1); col++)
                    {
                        if (tauler[row, col] == '.')
                        {
                            dotExistence++;
                        }
                    }
                }
                if (dotExistence == 0)
                {
                    tie(victory);
                }
            }
            endExercise();
        }

        /*Possibilitat d'empat*/
        void tie(bool flag)
        {
            if (flag == true)
            {
                Menu();
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Empat!");
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Pulsa una tecla per terminar");
                Console.ResetColor();
                Console.ReadLine();
                Menu();
            }
        }

        /*Guanyador del Tres en Linea: X*/
        void winX(bool bandera)
        {
            bandera = true;
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Guanyen les X!");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Pulsa para salir");
            Console.ResetColor();
            Console.ReadLine();
            Menu();
        }

        /*Guanyador del Tres en Linea: 0*/
        void win0(bool bandera)
        {
            bandera = true;
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Guanyen les 0!");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Pulsa para salir");
            Console.ResetColor();
            Console.ReadLine();
            Menu();
        }

        /*Torn del primer jugador*/
        void firstPlayer(char[,] tablero)
        {
            Console.Write("(Jugador 1) Escriu les coordenades: ");
            string coord = Console.ReadLine();
            int rowNum = (int)Char.GetNumericValue(coord[0]);

            while (rowNum < 0 || rowNum > 2)
            {
                Console.WriteLine("T'has passat dels limits en la primera coordenada");
                Console.Write("(Jugador 1) Escriu les coordenades: ");
                coord = Console.ReadLine();
                rowNum = (int)Char.GetNumericValue(coord[0]);
            }

            int colNum = (int)Char.GetNumericValue(coord[2]);

            while (colNum < 0 || colNum > 2)
            {
                Console.WriteLine("T'has passat dels limits en la segona coordenada");
                Console.Write("(Jugador 1) Escriu les coordenades: ");
                coord = Console.ReadLine();
                rowNum = (int)Char.GetNumericValue(coord[0]);
                colNum = (int)Char.GetNumericValue(coord[2]);
            }

            tablero[rowNum, colNum] = 'x';
            return;
        }

        /*Comparem els resultats del primer jugador per saber si guanya*/
        void compareFirst(char[,] tablero, bool flag)
        {
            // Files
            if (tablero[0, 0] == 'x' && tablero[0, 1] == 'x' && tablero[0, 2] == 'x')
            {
                winX(flag);
            }
            else if (tablero[1, 0] == 'x' && tablero[1, 1] == 'x' && tablero[1, 2] == 'x')
            {
                winX(flag);
            }
            else if (tablero[2, 0] == 'x' && tablero[2, 1] == 'x' && tablero[2, 2] == 'x')
            {
                winX(flag);
            }

            // Columnes
            if (tablero[0, 0] == 'x' && tablero[1, 0] == 'x' && tablero[2, 0] == 'x')
            {
                winX(flag);
            }
            else if (tablero[0, 1] == 'x' && tablero[1, 1] == 'x' && tablero[2, 1] == 'x')
            {
                winX(flag);
            }
            else if (tablero[0, 2] == 'x' && tablero[1, 2] == 'x' && tablero[2, 2] == 'x')
            {
                winX(flag);
            }

            // Diagonals
            if (tablero[0, 0] == 'x' && tablero[1, 1] == 'x' && tablero[2, 2] == 'x')
            {
                winX(flag);
            }
            else if (tablero[0, 2] == 'x' && tablero[1, 1] == 'x' && tablero[2, 0] == 'x')
            {
                winX(flag);
            }
        }

        /*Torn del segon jugador*/
        void secondPlayer(char[,] tablero)
        {
            Console.Write("(Jugador 2) Escriu les coordenades: ");
            string coord = Console.ReadLine();
            int rowNum = (int)Char.GetNumericValue(coord[0]);

            while (rowNum < 0 || rowNum > 2)
            {
                Console.WriteLine("T'has passat dels limits en la primera coordenada");
                Console.Write("(Jugador 2) Escriu les coordenades: ");
                coord = Console.ReadLine();
                rowNum = (int)Char.GetNumericValue(coord[0]);
            }


            int colNum = (int)Char.GetNumericValue(coord[2]);

            while (colNum < 0 || colNum > 2)
            {
                Console.WriteLine("T'has passat dels limits en la segona coordenada");
                Console.Write("(Jugador 2) Escriu les coordenades: ");
                coord = Console.ReadLine();
                colNum = (int)Char.GetNumericValue(coord[2]);
            }

            tablero[rowNum, colNum] = '0';
            return;
        }

        /*Comparem els resultats del segon jugador per saber si guanya*/
        void compareSecond(char[,] tablero, bool flag)
        {
            // Files
            if (tablero[0, 0] == '0' && tablero[0, 1] == '0' && tablero[0, 2] == '0')
            {
                win0(flag);
            }
            else if (tablero[1, 0] == '0' && tablero[1, 1] == '0' && tablero[1, 2] == '0')
            {
                win0(flag);
            }
            else if (tablero[2, 0] == '0' && tablero[2, 1] == '0' && tablero[2, 2] == '0')
            {
                win0(flag);
            }

            // Columnes
            if (tablero[0, 0] == '0' && tablero[1, 0] == '0' && tablero[2, 0] == '0')
            {
                win0(flag);
            }
            else if (tablero[0, 1] == '0' && tablero[1, 1] == '0' && tablero[2, 1] == '0')
            {
                win0(flag);
            }
            else if (tablero[0, 2] == '0' && tablero[1, 2] == '0' && tablero[2, 2] == '0')
            {
                win0(flag);
            }

            // Diagonals
            if (tablero[0, 0] == '0' && tablero[1, 1] == '0' && tablero[2, 2] == '0')
            {
                win0(flag);
            }
            else if (tablero[0, 2] == '0' && tablero[1, 1] == '0' && tablero[2, 0] == '0')
            {
                win0(flag);
            }
        }

        /*Mostrem la matriu*/
        void showMatrix(char[,] tablero)
        {
            for (int row = 0; row < tablero.GetLength(0); row++)
            {
                for (int col = 0; col < tablero.GetLength(1); col++)
                {
                    Console.Write(" " + tablero[row, col]);

                }
                Console.WriteLine();
            }
        }



        /*
         * Volem fer un marcador per un partit de squash.
         * L'usuari primer introduirà el número de partits a introduir.
         * De cada partit, escriurà qui guanya cada punt i finalitzarà amb una F.
         */
        public void SQUASHCOUNTER()
        {
            Console.Write("Escriu la quantitat de partits: ");
            int partits = Convert.ToInt32(Console.ReadLine());
            int[] aList = new int[partits];
            int[] bList = new int[partits];
            int scoreA = 0;
            int scoreB = 0;
            int set = 9;

            for (int i = 0; i < partits; i++)
            {
                Console.Write("Escriu qui guanya cada punt: ");
                string score = Console.ReadLine();
                int lastLetter = score.Length - 1;
                if (score.ToUpper()[lastLetter] == 'F')
                {
                    squash(score.ToUpper(), lastLetter, aList, bList, scoreA, scoreB, set, i);
                }
                else
                {
                    while (score.ToUpper()[lastLetter] != 'F')
                    {
                        Console.Write("Escriu com continua: ");
                        score += Console.ReadLine();
                        lastLetter = score.Length - 1;
                    }
                    squash(score, lastLetter, aList, bList, scoreA, scoreB, set, i);
                }
            }
            endExercise();
        }

        /*Comprovacions del partit*/
        void squash(string score, int lastLetter, int[] aList, int[] bList, int scoreA, int scoreB, int set, int i)
        {
            aCounter(score, lastLetter, aList, i);
            bCounter(score, lastLetter, bList, i);
            if (bList[i] == 0 && aList[i] == 10) aList[i]--;
            if (aList[i] == 0 && bList[i] == 10) bList[i]--;

            if (aList[i] == set && bList[i] == set + 1) bList[i]++;
            if (bList[i] == set && aList[i] == set + 1) aList[i]++;

            Console.Write($"{aList[i]}-{bList[i]}");

            if (aList[i] == set && bList[i] == set + 2) scoreB++;
            if (aList[i] == set + 2 && bList[i] == set) scoreA++;

            if (scoreA > 0 || scoreB > 0)
            {
                Console.Write($"\t{scoreA}-{scoreB}");
            }
            Console.WriteLine("");
            return;
        }

        /*Conta la quantitat de lletres A que n'hi han.*/
        void aCounter(string puntuacion, int ultimaLetra, int[] listaA, int forCounter)
        {
            int contadorA = 0;
            int contadorAux = 0;

            for (int i = 0; i < ultimaLetra; i++)
            {
                if (puntuacion[i] == 'A') contadorA++;
                if (puntuacion[i] == 'B' && puntuacion[i + 1] == 'A')
                {
                    int j = i + 1;
                    if (contadorA == 1) contadorA--;
                    if (contadorA == 2) contadorA--;
                    if (contadorA == 8) contadorA++;
                    contadorAux++;
                    for (; j < ultimaLetra; j++)
                    {
                        if (puntuacion[i] == 'A') contadorAux++;
                    }
                    break;
                }
            }

            if (contadorA == 1) contadorA--;
            if (contadorA == 2) contadorA--;
            if (contadorA == 8) contadorA++;

            listaA[forCounter] = contadorA;
            return;
        }

        /*Conta la quantitat de lletres B que n'hi han.*/
        void bCounter(string puntuacion, int ultimaLetra, int[] listaB, int forCounter)
        {
            int contadorB = 0;
            int contadorAux = 0;

            for (int i = 0; i < ultimaLetra; i++)
            {
                if (puntuacion[i] == 'B') contadorB++;
                if (puntuacion[i] == 'B' && puntuacion[i + 1] == 'A')
                {
                    int j = i + 1;
                    if (contadorB == 1) contadorB--;
                    if (contadorB == 2) contadorB--;

                    contadorAux++;
                    for (; j < ultimaLetra; j++)
                    {
                        if (puntuacion[i] == 'B') contadorAux++;
                    }
                    break;
                }
            }

            if (contadorB == 1) contadorB--;
            if (contadorB == 2) contadorB--;

            listaB[forCounter] = contadorB;
            return;
        }
    }
}
